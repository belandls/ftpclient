/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GetFileCommand.cpp
 * Author: Samuel
 * 
 * Created on February 14, 2016, 3:11 PM
 */

#include "GetFileCommand.h"
#include "Request.h"
#include <iostream>

GetFileCommand::GetFileCommand() {
}

GetFileCommand::GetFileCommand(const GetFileCommand& orig) {
}

GetFileCommand::~GetFileCommand() {
}

void GetFileCommand::cleanup() {
    _out_stream->close();
    delete _out_stream;
}

bool GetFileCommand::performDataTransfer() {

    int receivedData = 0;
    int res;
    char buffer[4096];
    while (receivedData < _size_of_data) {
        res = recv(getWorkingSocket(), buffer, 4096, 0);
        if (res == SOCKET_ERROR) {
            return false;
        } else {
            _out_stream->write(buffer, res);
            receivedData += res;
        }
    }
    _out_stream->close();
    return true;

}

bool GetFileCommand::performInitialNego() {

    int res;
    Request rr;
    char buffer[Request::REQUEST_SIZE];
    res = recv(getWorkingSocket(), buffer, Request::REQUEST_SIZE, 0);
    if (res == SOCKET_ERROR) {
        return false;
    }
    rr.fillFromRawByteRepresentation(buffer);
    if (rr.getRequestType() != Request::READY_TO_SEND) {
        return false;
    }
    _size_of_data = rr.getSizeOfData();
    if (_ready_to_recv) {
        Request sr(Request::READY_TO_RECV, 0);
        res = send(getWorkingSocket(), sr.getRawByteRepresentation(), Request::REQUEST_SIZE, 0);
        if (res == SOCKET_ERROR) {
            return false;
        }
    }else{
        Request sr(Request::UNEXPECTED_ERROR, 0);
        res = send(getWorkingSocket(), sr.getRawByteRepresentation(), Request::REQUEST_SIZE, 0);
        return false;
    }

    return true;
}

bool GetFileCommand::sendCommand(string addInfo) {

    int res;
    Request sr(Request::GET_FILE, 0, addInfo);
    res = send(getWorkingSocket(), sr.getRawByteRepresentation(), Request::REQUEST_SIZE, 0);
    if (res == SOCKET_ERROR) {
        return false;
    }
    Request rr;
    char buffer[Request::REQUEST_SIZE];
    res = recv(getWorkingSocket(), buffer, Request::REQUEST_SIZE, 0);
    if (res == SOCKET_ERROR) {
        return false;
    }
    rr.fillFromRawByteRepresentation(buffer);
    if (rr.getRequestType() != Request::COMMAND_SUCCESFULL) {
        if (rr.getRequestType() == Request::FILE_NOT_FOUND) {
            cout << "The server did not find the file." << endl;
        }
        return false;
    }
    _ready_to_recv = true;
    _out_stream = new ofstream(addInfo, ofstream::binary);
    if (!_out_stream->is_open()) {
        _ready_to_recv = false;
    }

    return true;
}

void GetFileCommand::showResult() {
    std::cout << "File received succesfully" << endl;
}


