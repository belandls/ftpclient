/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Client.h
 * Author: Samuel
 *
 * Created on February 9, 2016, 8:42 PM
 */

#ifndef CLIENT_H
#define CLIENT_H

#define _WIN32_WINNT 0x501
#include <WinSock2.h>
#include <ws2tcpip.h>
#include <iostream>

using namespace std;

class Client
{
private:
	const static char* DEFAULT_PORT;
	SOCKET _data_socket;
public:
	Client();
	~Client();
	bool init();
	bool connect(string);
        SOCKET getSocket();
        
};

#endif /* CLIENT_H */

