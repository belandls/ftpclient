/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ListFileCommand.cpp
 * Author: Samuel
 * 
 * Created on February 9, 2016, 8:47 PM
 */

#include "ListFileCommand.h"
#include "Request.h"
#include <iostream>

ListFileCommand::ListFileCommand() {
}

ListFileCommand::ListFileCommand(const ListFileCommand& orig) {
}

ListFileCommand::~ListFileCommand() {
}

bool ListFileCommand::performDataTransfer() {
    int res = recv(getWorkingSocket(), _recv_buffer, _recv_buffer_size, 0);

}

void ListFileCommand::showResult() {
    cout << "Here are the files on the server : " << endl;
    cout << _recv_buffer;
}

void ListFileCommand::cleanup() {
    delete[] _recv_buffer;
    _recv_buffer_size = 0;
}

bool ListFileCommand::performInitialNego() {
    SOCKET currentSocket = getWorkingSocket();
    int res;
    char reqBuffer[260];

    Request r;
    res = recv(currentSocket, reqBuffer, 260, 0);
    if (res == SOCKET_ERROR) {
        return false;
    }
    r.fillFromRawByteRepresentation(reqBuffer);
    if (r.getRequestType() == Request::READY_TO_SEND) {

        _recv_buffer = new char[r.getSizeOfData()];
        _recv_buffer_size = r.getSizeOfData();

        Request sr(Request::READY_TO_RECV, 0);
        res = send(currentSocket, sr.getRawByteRepresentation(), 260, 0);
        if (res == SOCKET_ERROR) {
            return false;
        }
    } else {
        return false;
    }
    return true;

}

bool ListFileCommand::sendCommand(string addInfo) {
    Request r(Request::LIST_FILES, 0);
    int res = send(getWorkingSocket(), r.getRawByteRepresentation(), 260, 0);
    if (res == SOCKET_ERROR) {
        cout << WSAGetLastError() << endl;
        return false;
    }
    char buffer[260];
    res = recv(getWorkingSocket(), buffer, 260, 0);
    if (res == SOCKET_ERROR) {
        cout << WSAGetLastError() << endl;
        return false;
    }
    Request rr;
    rr.fillFromRawByteRepresentation(buffer);
    if (rr.getRequestType() != Request::COMMAND_SUCCESFULL) {
        return false;
    }
    return true;
}


