/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GetFileCommand.h
 * Author: Samuel
 *
 * Created on February 14, 2016, 3:11 PM
 */

#ifndef GETFILECOMMAND_H
#define GETFILECOMMAND_H

#include "Command.h"
#include <fstream>
using namespace std;
class GetFileCommand : public Command {
public:
    GetFileCommand();
    GetFileCommand(const GetFileCommand& orig);
    virtual ~GetFileCommand();
    
    void cleanup() override;
    bool performDataTransfer() override;
    bool performInitialNego() override;
    bool sendCommand(string) override;
    void showResult() override;

private:
    int _size_of_data;
    ofstream* _out_stream;
    bool _ready_to_recv;
};

#endif /* GETFILECOMMAND_H */

