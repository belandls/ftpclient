/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ListFileCommand.h
 * Author: Samuel
 *
 * Created on February 9, 2016, 8:47 PM
 */

#ifndef LISTFILECOMMAND_H
#define LISTFILECOMMAND_H

#include "Command.h"


class ListFileCommand : public Command {
public:
    ListFileCommand();
    ListFileCommand(const ListFileCommand& orig);
    virtual ~ListFileCommand();
    
    bool sendCommand(string) override;
    bool performDataTransfer() override;
    bool performInitialNego() override;
    void showResult() override;
    void cleanup() override;



private:
    char* _recv_buffer;
    int _recv_buffer_size;
};

#endif /* LISTFILECOMMAND_H */

